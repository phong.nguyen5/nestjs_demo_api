import { User } from './user.entity';
import { CreateUserDto } from './dto/create-user.dto';
export declare class UsersService {
    create(createUserDto: CreateUserDto): Promise<User>;
    showById(id: number): Promise<User>;
    findById(id: number): Promise<User>;
    findByEmail(email: string): Promise<User>;
}
